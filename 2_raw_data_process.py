#!/usr/bin/env python3
# coding: utf-8
"""
Coding:
    Sylvain Théry - UMR 5281 ART-Dev - 2022/2023
    CC-by-SA-NC
Algorithm:
    Pierre Gautereau - UMR 8586 Prodig - Paris, France
    Ludivine Eloy - UMR 5281 ART-Dev - Montpellier, France
    Sylvain Théry - UMR 5281 ART-Dev - Montpellier, France
"""
import glob
import logging
import os
import shutil
from datetime import datetime
import sys
from PyQt5.QtCore import QVariant
from qgis._core import QgsProcessingFeatureSourceDefinition, QgsFeatureRequest
from qgis.core import QgsField, QgsVectorLayer, QgsApplication, QgsVectorFileWriter, QgsFeature, QgsWkbTypes

# params
source_path = '/home/sylvain/Work/ART-Dev/2022/2022-00-00_Bresil/2022-10-13/1_raw_data/'
dest_path = '/home/sylvain/Work/prog/2022-02-22_brazil_cerrado/2022-00-00_Bresil/2022-10-13/2_cleaned_data/'
id_epsg=29101 # "SAD69 / Brazil Polyconic". seems to be be something wrong with "SIRGAS 2000" EPSG 4988


#
# /!\ U should not change things below /!\
#
log_level = logging.INFO
os.makedirs(os.path.dirname(dest_path), exist_ok=True)
result_file_path = dest_path + datetime.now().strftime('%Y-%m-%dT%Hh%M.%S') + "_results.csv"
log_file_path = dest_path + datetime.now().strftime('%Y-%m-%dT%Hh%M.%S') + "_run.log"

# logging stuff
logging.basicConfig(level=log_level,
                    filename=log_file_path,
                    filemode="a",
                    format='%(asctime)s - %(levelname)s - %(message)s')

# QGIS stuff
# See https://gis.stackexchange.com/a/155852/4972 for details about the prefix
QgsApplication.setPrefixPath('/usr', True)
qgs = QgsApplication([], False)
qgs.initQgis()
# Append the path where processing plugin can be found
sys.path.append('/usr/share/qgis/python/plugins/')
import processing
from processing.core.Processing import Processing

Processing.initialize()

def clean_up_gpkg_tmp_files(path):
    for dirpath, dirnames, filenames in os.walk(path):
        for filename in filenames:
            if filename.endswith('.gpkg-shm') or filename.endswith('.gpkg-wal'):
                filepath = os.path.join(dirpath, filename)
                os.remove(filepath)

def list_params(source_path):
    """

    :param source_path:
    :return:
    """
    params_list = []
    for dir in glob.glob(source_path + "/*/"):
        for sub_dir in glob.glob(dir + "/*/"):
            # itération sur les municipios

            for sub_sub_dir in glob.glob(sub_dir + "/*/"):
                params_list.append(os.path.basename(sub_sub_dir[:-1]))
            params_list = list(set(params_list))
    return params_list


def realcentroid(layer, dest):
    """ generate point on surface
    taken and adaptated from Zoltan Siki's RealCentroid plugin
    https://github.com/zsiki/realcentroid/
    """
    vlayer = QgsVectorLayer(layer)
    vprovider = vlayer.dataProvider()
    dest = os.path.splitext(dest)[0]
    writer = QgsVectorFileWriter(dest, 'UTF-8', vprovider.fields(), QgsWkbTypes.Point, \
                                 vprovider.crs(), "GPKG")
    # options = QgsVectorFileWriter.SaveVectorOptions()
    # options.driverName = "GPKG"
    # options.layerName = dest
    # options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteFile
    # options.fileEncoding = "UTF-8"
    # options.layerOptions = ['GEOMETRY_NAME=geom']
    #
    # QgsVectorFileWriter.writeAsVectorFormatV3(vlayer, dest, options)

    outFeat = QgsFeature()

    features = vlayer.getFeatures()
    nElement = 0
    nError = 0
    for inFeat in features:
        nElement += 1
        inGeom = inFeat.geometry()
        if inGeom is None or not inFeat.hasGeometry() or \
                not inGeom.isGeosValid():
            nError += 1
            continue
        if inGeom.isMultipart():
            # find largest part in case of multipart
            maxarea = 0
            # tmpGeom = QgsGeometry()
            for part in inGeom.asGeometryCollection():
                area = part.area()
                if area > maxarea:
                    tmpGeom = part
                    maxarea = area
            inGeom = tmpGeom
        atMap = inFeat.attributes()
        outGeom = inGeom.pointOnSurface()
        outFeat.setAttributes(atMap)
        outFeat.setGeometry(outGeom)
        writer.addFeature(outFeat)
    del writer





def A1_0_data_preprocessing():
    with open(result_file_path, "a") as result_file:
        # result file header
        result_file.write('state;municipio;param;raw_nb_records;after_fixing;after_single_part; after_dedoublon; after_fixing_dedoublon\n')
        for working_dir in sorted(glob.glob(source_path + "/*/")):
            state = os.path.basename(working_dir[:-1])

            for sub_dir in sorted(glob.glob(working_dir + "/*/")):
                # Iterate on municipios
                municipio = os.path.basename(sub_dir[:-1])

                # iterate on params name
                for sub_sub_dir in sorted(glob.glob(sub_dir + "/*/")):
                    # iterate on params name
                    param = os.path.basename(sub_sub_dir[:-1])

                    # print(f'{state} {municipio} {param}')
                    # shortcut for logging
                    smp = f'{state} {municipio} {param}'
                    shp_src = (sub_sub_dir + param + ".shp")
                    shp_src_layer = QgsVectorLayer(shp_src)
                    if shp_src_layer.isValid():
                        if shp_src_layer.featureCount() >= 1:
                            # beginning of result file line to be written
                            result_string = f"{state}; {municipio}; {param};"
                            # fix geometries
                            try:
                                tmp_result_string, gpkg_geom_fixed_name = A_1_0_0_fixgeom(municipio, param, shp_src,
                                                                                          shp_src_layer, smp, state)
                                result_string += tmp_result_string
                            except:
                                print(f'Issue when trying to fix geometries of {smp}...')
                                logging.error(f'Issue when trying to fix geometries of {smp}...')

                            # convert to single part
                            try:
                                # print(f'{smp} single part')
                                tmp_result_string, gpkg_single_part_name = A_1_0_1_multiparts2single(gpkg_geom_fixed_name,
                                                                                                     municipio, param, state)
                                result_string += tmp_result_string
                            except:
                                print(f'Issue when trying to convert {smp} into single part...')
                                logging.error(f'Issue when trying to convert {smp} into single part...')

                            # deduplicate geom
                            try:
                                # print(f'{smp} deduplicate')
                                tmp_result_string, gpkg_deduplicate_name = A_1_0_2_deduplicate_geom(gpkg_single_part_name,
                                                                                                    municipio, param, state)
                                result_string += tmp_result_string
                            except:
                                print(f'Issue when trying to delete duplicates geometries of {smp}...')
                                logging.error(f'Issue when trying to delete duplicates geometries of {smp}...')


                            # add geometry metrics columns after reproject
                            try:

                                tmp_result_string, gpkg_with_geom_column_name = A_1_0_3_project_and_add_geom_cols(gpkg_deduplicate_name, municipio,
                                                                                                   param, state)
                                result_string += tmp_result_string
                            except:
                                print(f'Issue when trying to add geometry columns to {smp}...')
                                logging.error(f'Issue when trying to add geometry columns to {smp}...')

                            # convert polygons to point for further spatial joins
                            try:
                                # print(f'{smp} convert poylgones to points')
                                A_1_0_4_convert_polygons2points(gpkg_with_geom_column_name, municipio, param, state)
                            except:
                                print(f'Issue when trying to convert {smp} to point...')
                                logging.error(f'Issue when trying to convert {smp} to point...')
                            result_file.write(result_string + '\n')
                        else:
                            print(f'    Empty shapefile {smp}...')
                            logging.error(f'    Empty shapefile {smp}...')
                    else:
                        print(f'    Invalid shapefile {smp}...')
                        logging.error(f'    Invalid shapefile {smp}...')




def A_1_0_0_fixgeom(municipio, param, shp_src, shp_src_layer, smp, state):
    if shp_src_layer.featureCount() >= 1:
        # now sure that we have a not empty and valid QgsVectorLayer

        # add fields to keep track of origin
        # (special name 4 Area Imovel because of the final jointure)
        if param == "AREA_IMOVEL":
            list_fields = ['state_AI', 'munici_AI', 'param_AI']
        else:
            list_fields = ['state', 'municipio', 'param']
        list_values = [state, municipio, param]

        shp_src_layer_provider = shp_src_layer.dataProvider()
        shp_src_layer.startEditing()
        for field in list_fields:
            shp_src_layer_provider.addAttributes([QgsField(field, QVariant.String)])
        shp_src_layer.updateFields()

        shp_src_layer.startEditing()
        for feature in shp_src_layer.getFeatures():
            for i in range(3):
                feature.setAttribute(list_fields[i], list_values[i])
            shp_src_layer.updateFeature(feature)
        shp_src_layer.commitChanges()

        # define gpkg dest name and create dest path if not exists
        gpkg_geom_fixed_name = f'{dest_path}/00_geom_fixed/{state}/{param}_{municipio}.gpkg'
        os.makedirs(os.path.dirname(gpkg_geom_fixed_name), exist_ok=True)

        # add value to result string
        result_string = f"{shp_src_layer.featureCount()};"

        # fix geometries
        processing.run("native:fixgeometries", {
            'INPUT': shp_src,
            'OUTPUT': gpkg_geom_fixed_name})
        gpkg_geom_fixed_layer = QgsVectorLayer(gpkg_geom_fixed_name)
        result_string += f"{gpkg_geom_fixed_layer.featureCount()};"
        return result_string, gpkg_geom_fixed_name

    else:
        logging.error(f'Empty shapefile {smp}...')


def A_1_0_1_multiparts2single(gpkg_geom_fixed_name, municipio, param, state):
    
    # convert multiparts to single parts for all params but 'RESERVA_LEGAL
    gpkg_single_part_name = f'{dest_path}/01_single_part/{state}/{param}_{municipio}.gpkg'
    os.makedirs(os.path.dirname(gpkg_single_part_name), exist_ok=True)
    if os.path.basename(gpkg_geom_fixed_name)[:13] == 'RESERVA_LEGAL':
        #in this case, we just copy the gpkg
        shutil.copy(gpkg_geom_fixed_name, gpkg_single_part_name)
    else:
        processing.run("native:multiparttosingleparts", {'INPUT': gpkg_geom_fixed_name,
                                                         'OUTPUT': gpkg_single_part_name})
    gpkg_single_part_layer = QgsVectorLayer(gpkg_single_part_name)
    result_string = f"{gpkg_single_part_layer.featureCount()};"
    return result_string, gpkg_single_part_name


def A_1_0_2_deduplicate_geom(gpkg_single_part_name, municipio, param, state):
    # geometrically deduplicate features of the layer
    gpkg_deduplicate_name = f'{dest_path}/02_deduplicate/{state}/{param}_{municipio}.gpkg'
    os.makedirs(os.path.dirname(gpkg_deduplicate_name), exist_ok=True)
    processing.run("native:deleteduplicategeometries", {
        'INPUT': gpkg_single_part_name,
        'OUTPUT': gpkg_deduplicate_name})
    gpkg_deduplicate_layer = QgsVectorLayer(gpkg_deduplicate_name)
    result_string = f"{gpkg_deduplicate_layer.featureCount()};"
    return result_string, gpkg_deduplicate_name


def A_1_0_3_project_and_add_geom_cols(gpkg_deduplicate_name, municipio, param, state):
    gpkg_with_projected_name = f'{dest_path}/03_projected/{state}/{param}_{municipio}.gpkg'
    os.makedirs(os.path.dirname(gpkg_with_projected_name), exist_ok=True)
    params = {
        'INPUT': QgsProcessingFeatureSourceDefinition(gpkg_deduplicate_name, selectedFeaturesOnly=False, featureLimit=-1, flags=QgsProcessingFeatureSourceDefinition.FlagOverrideDefaultGeometryCheck, geometryCheck=QgsFeatureRequest.GeometrySkipInvalid),
        'TARGET_CRS': f'EPSG:{id_epsg}',
        'OUTPUT': gpkg_with_projected_name
    }

    # reprojection
    result = processing.run("native:reprojectlayer", params)

    gpkg_with_geom_column_name = f'{dest_path}/04_with_geom_col/{state}/{param}_{municipio}.gpkg'
    os.makedirs(os.path.dirname(gpkg_with_geom_column_name), exist_ok=True)

    processing.run("qgis:exportaddgeometrycolumns", {'INPUT': QgsProcessingFeatureSourceDefinition(
        gpkg_with_projected_name,
        selectedFeaturesOnly=False, featureLimit=-1,
        flags=QgsProcessingFeatureSourceDefinition.FlagOverrideDefaultGeometryCheck,
        geometryCheck=QgsFeatureRequest.GeometrySkipInvalid), 'CALC_METHOD': 0, 'OUTPUT': gpkg_with_geom_column_name})
    gpkg_with_geom_column_layer = QgsVectorLayer(gpkg_with_geom_column_name)
    result_string = f"{gpkg_with_geom_column_layer.featureCount()};"
    return result_string, gpkg_with_geom_column_name


def A_1_0_4_convert_polygons2points(gpkg_with_geom_column_name, municipio, param, state):
    gpkg_geom_convert_name = f'{dest_path}/05_geom_changed/{state}/{param}_{municipio}.gpkg'
    os.makedirs(os.path.dirname(gpkg_geom_convert_name), exist_ok=True)

    if os.path.basename(gpkg_with_geom_column_name)[:19] == 'NASCENTE_OLHO_DAGUA' \
            or os.path.basename(gpkg_with_geom_column_name)[:11] == 'AREA_IMOVEL':
        shutil.copy(gpkg_with_geom_column_name, gpkg_geom_convert_name)
    else:

        # create points from polygons. Points has to be inside polygons
        # (!= centroid)
        realcentroid(gpkg_with_geom_column_name, gpkg_geom_convert_name)


def A1_1_merge_params_by_state():
    """

    :return:
    """
    params = sorted(list_params(source_path))

    for directory in glob.glob(f'{dest_path}/05_geom_changed/*/'):
        state = os.path.basename(directory[:-1])
        os.makedirs(os.path.dirname(f'{dest_path}/06_data_merged_by_state/'), exist_ok=True)
        for param in params:
            # print(param)
            # print(glob.glob(directory + f"/{param}*.gpkg"))
            if len(glob.glob(directory + f"{param}*.gpkg")) > 0:
                # try:
                processing.run("native:mergevectorlayers", {
                    'LAYERS': glob.glob(directory + f"/{param}*.gpkg"),
                    'OUTPUT': f'ogr:dbname=\'{dest_path}/06_data_merged_by_state/{state}.gpkg\' table="{param}" (geom)'})
                # except:
                #     print(f'Issue when trying to concatenate  {param} files for {state}...')
                #     logging.error(f'Issue when trying to concatenate  {param} files for {state}...')


def check_geometry():
    # Spécifier le répertoire des fichiers GPKG
    directory = f'{dest_path}05_geom_changed/Bahia/'

    # Ouvrir un fichier texte pour écrire les résultats
    with open(f'{directory}geom_type.txt', 'w') as f:
        # Parcourir tous les fichiers GPKG dans le répertoire
        for filename in sorted(os.listdir(directory)):
            if filename.endswith('.gpkg'):
                filepath = os.path.join(directory, filename)
                # Charger le fichier GPKG en tant que couche vectorielle
                layer = QgsVectorLayer(filepath, '', 'ogr')
                if not layer.isValid():
                    print("La couche n'est pas valide : ", filepath)
                else:
                    # Écrire le nom de la couche et son type de géométrie dans le fichier texte
                    geometry_type = QgsWkbTypes.geometryDisplayString(layer.geometryType())
                    f.write(f'{os.path.basename(filename)};{layer.name()}; {geometry_type}\n')

if __name__ == '__main__':
    beginning = datetime.now()
    logging.info(f'Process starts @ {datetime.now().strftime("%Y-%m-%dT%Hh%M.%S")}')
    # A0_unzip(source_path, True)
    # Iterate through zip file and write results
    A1_0_data_preprocessing()
    #A1_1_merge_params_by_state()
    clean_up_gpkg_tmp_files(dest_path)
    logging.info(f'Process finishes @ {datetime.now().strftime("%Y-%m-%dT%Hh%M.%S")}')
