#!/usr/bin/env python3
# coding: utf-8
"""
Coding:
    Sylvain Théry - UMR 5281 ART-Dev - 2022/2023
    CC-by-SA-NC
Algorithm:
    Pierre Gautereau - UMR 8586 Prodig - Paris, France
    Ludivine Eloy - UMR 5281 ART-Dev - Montpellier, France
    Sylvain Théry - UMR 5281 ART-Dev - Montpellier, France
"""
import glob
import logging
import os
import shutil
from datetime import datetime
import sys
from PyQt5.QtCore import QVariant
from qgis._core import QgsSpatialIndex, QgsFeatureRequest, QgsProject, QgsCoordinateTransformContext
from qgis.core import QgsField, QgsVectorLayer, QgsApplication, QgsVectorFileWriter, QgsFeature, QgsWkbTypes

# params
source_path = '/home/sylvain/Work/ART-Dev/2022/2022-00-00_Bresil/2022-10-13/2_cleaned_data/06_data_merged_by_state/'
dest_path = '/home/sylvain/Work/ART-Dev/2022/2022-00-00_Bresil/2022-10-13/3_imovel_process/'
percent=15
log_level = logging.WARNING

#
# /!\ U should not change things below /!\
#
os.makedirs(os.path.dirname(dest_path), exist_ok=True)
result_file_path = dest_path + datetime.now().strftime('%Y-%m-%dT%Hh%M.%S') + "_results.csv"
log_file_path = dest_path + datetime.now().strftime('%Y-%m-%dT%Hh%M.%S') + "_run.log"

# logging stuff
logging.basicConfig(level=log_level,
                    filename=log_file_path,
                    filemode="a",
                    format='%(asctime)s - %(levelname)s - %(message)s')

# QGIS stuff
# See https://gis.stackexchange.com/a/155852/4972 for details about the prefix
QgsApplication.setPrefixPath('/usr', True)
qgs = QgsApplication([], False)
qgs.initQgis()
# Append the path where processing plugin can be found
sys.path.append('/usr/share/qgis/python/plugins/')
import processing
from processing.core.Processing import Processing

Processing.initialize()

def A1_4_b_add_overlap_values_of_polys_contained_in_one_poly(percent):
    source_path_for_input =f"{dest_path}/06_select_immovel_contained_in/"
    for source_file_path in glob.glob(source_path_for_input + "/*.gpkg"):
        # Copy Imovel poly_layer

        input_layer = QgsVectorLayer(f"{source_file_path[:-27]}_imovel_poly_contained.gpkg|layername=AREA_IMOVEL_selected_because_not_contained_any", "AREA_IMOVEL_selected_because_not_contained_any", "ogr")
        print(input_layer)
        # Définir les options d'écriture pour écrire la couche dans un nouveau GPKG
        options = QgsVectorFileWriter.SaveVectorOptions()
        options.driverName = "GPKG"
        options.layerName = "AREA_IMOVEL_with_overlap_values"
        options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteFile
        options.EditionCapability = QgsVectorFileWriter.CanAddNewLayer
        options.fileEncoding = "utf-8"

        # write new gpkg
        dest_file_path= f"{dest_path}/07_select_overlap/{os.path.basename(source_file_path)[:-27]}_imovel_overlap.gpkg"
        print(dest_file_path)
        os.makedirs(os.path.dirname(dest_file_path), exist_ok=True)
        QgsVectorFileWriter.writeAsVectorFormat(input_layer, dest_file_path, options)

        poly_layer = QgsVectorLayer(f"{dest_file_path}|layername=AREA_IMOVEL_with_overlap_values", "AREA_IMOVEL", "ogr")
        if not poly_layer.isValid():
            print("Layer failed to load!")


        # Ajdd new field for overlap area and percent
        poly_layer.dataProvider().addAttributes([QgsField("OverlapPct", QVariant.Double)])
        poly_layer.dataProvider().addAttributes([QgsField("OverlapArea", QVariant.Double)])
        poly_layer.updateFields()

        # Union of polygons
        overlapLayer = processing.run("native:union", {'INPUT': poly_layer, 'OVERLAY': poly_layer, 'OUTPUT': 'memory:'})

        # Calculate overlap area and percent
        poly_layer.startEditing()
        for feature in poly_layer.getFeatures():
            overlap = 0
            geom = feature.geometry()
            for overlapFeature in overlapLayer['OUTPUT'].getFeatures():
                if overlapFeature.geometry().intersects(geom):
                    overlap += overlapFeature.geometry().intersection(geom).area()
            overlap=overlap-geom.area() # we don't count the feature itself
            overlapPct = (overlap / geom.area()) * 100
            # Assigner les valeurs pour les nouveaux champs
            poly_layer.changeAttributeValue(feature.id(), poly_layer.fields().indexOf("OverlapPct"), overlapPct)
            poly_layer.changeAttributeValue(feature.id(), poly_layer.fields().indexOf("OverlapArea"), overlap)
        poly_layer.commitChanges()
        crs = poly_layer.crs()

        # Create two memory layers to store feature with only 1 containig poly (itself) and another for the other
        select_layer = QgsVectorLayer("Polygon?crs=" + crs.authid(), f"AREA_IMOVEL_selected_because_less_than_{str(percent)}", "memory")
        not_select_layer = QgsVectorLayer("Polygon?crs=" + crs.authid(), "AREA_IMOVEL_not_selected_because_to_much_overlap", "memory")

        # Getting input layer fields list
        provider = poly_layer.dataProvider()
        fields = provider.fields()

        # add list fileds to memory layers
        select_provider = select_layer.dataProvider()
        select_provider.addAttributes(fields)
        select_layer.updateFields()

        not_select_provider = not_select_layer.dataProvider()
        not_select_provider.addAttributes(fields)
        not_select_layer.updateFields()

        # Filter by the number of complitly contains number of polygons
        request = QgsFeatureRequest().setFilterExpression(f"OverlapPct <= {str(percent)}")
        for feat in poly_layer.getFeatures(request):
            select_provider.addFeature(feat)

        request = QgsFeatureRequest().setFilterExpression(f"OverlapPct > {str(percent)}")
        for feat in poly_layer.getFeatures(request):
            not_select_provider.addFeature(feat)

        # Save layers in the same geopackage .gpkg
        select_layer.commitChanges()
        not_select_layer.commitChanges()

        options = QgsVectorFileWriter.SaveVectorOptions()
        options.driverName = "GPKG"
        options.fileEncoding = "utf-8"
        options.layerName = select_layer.name()
        options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteLayer
        options.EditionCapability = QgsVectorFileWriter.CanAddNewLayer

        QgsVectorFileWriter.writeAsVectorFormatV3(select_layer, dest_file_path, QgsCoordinateTransformContext(), options)

        options.layerName = not_select_layer.name()
        QgsVectorFileWriter.writeAsVectorFormatV3(not_select_layer, dest_file_path, QgsCoordinateTransformContext(), options)


def A1_4_a_add_number_of_polys_contained_in_one_poly():

    for source_file_path in glob.glob(source_path + "/*.gpkg"):
        # Copy Imovel layer
        input_layer = QgsVectorLayer(f"{source_file_path}|layername=AREA_IMOVEL", "AREA_IMOVEL", "ogr")
        # Définir les options d'écriture pour écrire la couche dans un nouveau GPKG
        options = QgsVectorFileWriter.SaveVectorOptions()
        options.driverName = "GPKG"
        options.layerName = "AREA_IMOVEL_with_count"
        options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteFile
        options.EditionCapability = QgsVectorFileWriter.CanAddNewLayer
        options.fileEncoding = "utf-8"

        # write new gpkg
        dest_file_path= f"{dest_path}/06_select_immovel_contained_in/{os.path.basename(source_file_path)[:-5]}_imovel_poly_contained.gpkg"
        print(dest_file_path)
        os.makedirs(os.path.dirname(dest_file_path), exist_ok=True)
        QgsVectorFileWriter.writeAsVectorFormat(input_layer, dest_file_path, options)
        poly_layer = QgsVectorLayer(f"{dest_file_path}|layername=AREA_IMOVEL_with_count", "AREA_IMOVEL", "ogr")
        # Check if the layer is valid
        if not poly_layer.isValid():
            print("Layer failed to load!")
            return False
        # Create a new field
        fields = poly_layer.fields()
        fields.append(QgsField("contained_count", QVariant.Int, "integer", 10))
        poly_layer.dataProvider().addAttributes([QgsField("contained_count", QVariant.Int)])
        poly_layer.updateFields()

        # Iterate over the polygons and set the default value for the new field
        poly_layer.startEditing()
        for f in poly_layer.getFeatures():
            poly_layer.changeAttributeValue(f.id(), fields.indexFromName("contained_count"), 0)
        poly_layer.commitChanges()

        # Create a spatial index for the layer
        index = QgsSpatialIndex()
        for f in poly_layer.getFeatures():
            index.insertFeature(f)

        # Iterate over the polygons and count the number of contained polygons
        poly_layer.startEditing()
        for f in poly_layer.getFeatures():
            geom = f.geometry()
            contained_count = 0
            for id in index.intersects(geom.boundingBox()):
                other_f = next(poly_layer.getFeatures(QgsFeatureRequest(id)))
                if geom.contains(other_f.geometry()):
                    contained_count += 1

            contained_count -= 1
            poly_layer.changeAttributeValue(f.id(), fields.indexFromName("contained_count"), contained_count)
        poly_layer.commitChanges()
        crs = poly_layer.crs()

        # Create two memory layers to store feature with only 1 containig poly (itself) and another for the other
        select_layer = QgsVectorLayer("Polygon?crs=" + crs.authid(), "AREA_IMOVEL_selected_because_not_contained_any", "memory")
        not_select_layer = QgsVectorLayer("Polygon?crs=" + crs.authid(), "AREA_IMOVEL_not_selected_because_containing_many_others", "memory")

        # Getting input layer fields list
        provider = poly_layer.dataProvider()
        fields = provider.fields()

        # add list fileds to memory layers
        select_provider = select_layer.dataProvider()
        select_provider.addAttributes(fields)
        select_layer.updateFields()

        not_select_provider = not_select_layer.dataProvider()
        not_select_provider.addAttributes(fields)
        not_select_layer.updateFields()

        # Filter by the number of complitly contains number of polygons
        request = QgsFeatureRequest().setFilterExpression("contained_count = 0")
        for feat in poly_layer.getFeatures(request):
            select_provider.addFeature(feat)

        request = QgsFeatureRequest().setFilterExpression("contained_count > 0")
        for feat in poly_layer.getFeatures(request):
            not_select_provider.addFeature(feat)

        # Enregistrer les nouvelles couches dans le même fichier .gpkg
        select_layer.commitChanges()
        not_select_layer.commitChanges()

        options = QgsVectorFileWriter.SaveVectorOptions()
        options.driverName = "GPKG"
        options.fileEncoding = "utf-8"
        options.layerName = select_layer.name()
        options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteLayer
        options.EditionCapability = QgsVectorFileWriter.CanAddNewLayer

        QgsVectorFileWriter.writeAsVectorFormatV3(select_layer, dest_file_path, QgsCoordinateTransformContext(), options)

        options.layerName = not_select_layer.name()
        QgsVectorFileWriter.writeAsVectorFormatV3(not_select_layer, dest_file_path, QgsCoordinateTransformContext(), options)

if __name__ == '__main__':
    A1_4_a_add_number_of_polys_contained_in_one_poly()
    A1_4_b_add_overlap_values_of_polys_contained_in_one_poly(percent)
