#!/usr/bin/env python3
# coding: utf-8
"""
Coding:
    Sylvain Théry - UMR 5281 ART-Dev - 2022/2023
    CC-by-SA-NC
Algorithm:
    Pierre Gautereau - UMR 8586 Prodig - Paris, France
    Ludivine Eloy - UMR 5281 ART-Dev - Montpellier, France
    Sylvain Théry - UMR 5281 ART-Dev - Montpellier, France
"""
import glob
import logging
import os
import shutil
from datetime import datetime
import sys
from PyQt5.QtCore import QVariant
from qgis._core import QgsSpatialIndex, QgsFeatureRequest, QgsProject, QgsCoordinateTransformContext, \
    QgsCoordinateReferenceSystem, QgsFields, QgsProcessingFeatureSourceDefinition
from qgis.core import QgsField, QgsVectorLayer, QgsApplication, QgsVectorFileWriter, QgsFeature, QgsWkbTypes
from collections import defaultdict
# params
source_path = '/home/sylvain/Work/ART-Dev/2022/2022-00-00_Bresil/2022-10-13/2_cleaned_data/05_geom_changed/'
dest_path = '/home/sylvain/Work/ART-Dev/2022/2022-00-00_Bresil/2022-10-13/3_imovel_process/'
percent_overlap=15
min_percent_AI_RL = 0.18
max_percent_AI_RL = 0.22
log_level = logging.INFO

#
# /!\ U should not change things below /!\
#
os.makedirs(os.path.dirname(dest_path), exist_ok=True)
result_file_path = dest_path + datetime.now().strftime('%Y-%m-%dT%Hh%M.%S') + "_results.csv"
log_file_path = dest_path + datetime.now().strftime('%Y-%m-%dT%Hh%M.%S') + "_run.log"

# logging stuff
logging.basicConfig(level=log_level,
                    filename=log_file_path,
                    filemode="a",
                    format='%(asctime)s - %(levelname)s - %(message)s')

# QGIS stuff
# See https://gis.stackexchange.com/a/155852/4972 for details about the prefix
QgsApplication.setPrefixPath('/usr', True)
qgs = QgsApplication([], False)
qgs.initQgis()
# Append the path where processing plugin can be found
sys.path.append('/usr/share/qgis/python/plugins/')
import processing
from processing.core.Processing import Processing

Processing.initialize()


def create_empty_gpkg_layer(gpkg_path: str, layer_name: str, geometry: int,
                            crs: str, schema: QgsFields, append: bool = False
                            ) -> None:
    """
    Create a empty layer into a gpkg file. The gpkg is created if needed, and can be overwritten if it already exists
    Taken from :
    https://gis.stackexchange.com/questions/417916/creating-empty-layers-in-a-geopackage-using-pyqgis
    Thanks to Germán Carrillo https://gis.stackexchange.com/users/4972/germ%c3%a1n-carrillo

    :param gpkg_path: geopackage file
    :type gpkg_path: str

    :param layer_name: layer to be created
    :type layer_name: str

    :param geometry: Geometry Type. Can be none.
    :type geometry: QgsWkbType

    :param crs: CRS of the geometry. Can be empty
    :type crs: str

    :param schema: Attribute table structure
    :type schema: QgsFields()

    :param append: What to do when gpkg file exists (create or overwrite layer)
    :type append: bool

    :return: None
    :rtype: None
    """
    os.makedirs((os.path.dirname(gpkg_path)), exist_ok=True)
    options = QgsVectorFileWriter.SaveVectorOptions()
    options.driverName = "GPKG"
    options.layerName = layer_name
    if append:
        options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteLayer

    writer = QgsVectorFileWriter.create(
        gpkg_path,
        schema,
        geometry,
        QgsCoordinateReferenceSystem(crs),
        QgsCoordinateTransformContext(),
        options)
    del writer



def A1_create_dict_of_param_by_municipio(path):
    """

    """
    d = defaultdict(list)
    for filename in os.listdir(path):
        if filename.endswith('.gpkg'):
            left, right = filename.split('.')[0].rsplit("_",1)
            d[right].append(left)

    # trier les listes de valeurs
    for key in d:
        d[key] = sorted(d[key])

    return d




def A1_4_a_add_number_of_polys_contained_in_one_poly(input_layer, state):
    # Copy Imovel layer
    # Définir les options d'écriture pour écrire la couche dans un nouveau GPKG
    print("    Dealing with poly inside poly")
    options = QgsVectorFileWriter.SaveVectorOptions()
    options.driverName = "GPKG"
    options.layerName = "AREA_IMOVEL_with_count"
    options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteFile
    options.EditionCapability = QgsVectorFileWriter.CanAddNewLayer
    options.fileEncoding = "utf-8"

    # write new gpkg
    dest_file_path= f"{dest_path}/06_select_immovel_contained_in/{state}/{os.path.basename(input_layer.name())}_imovel_poly_contained.gpkg"

    os.makedirs(os.path.dirname(dest_file_path), exist_ok=True)
    new_layer = QgsVectorFileWriter.writeAsVectorFormatV3(input_layer, dest_file_path,QgsCoordinateTransformContext(), options)
    poly_layer = QgsVectorLayer(f"{dest_file_path}|layername=AREA_IMOVEL_with_count", "AREA_IMOVEL", "ogr")
    # Check if the layer is valid
    if not poly_layer.isValid():
        print("Layer failed to load!")
        return False
    # Create a new field
    fields = poly_layer.fields()
    fields.append(QgsField("contained_count", QVariant.Int, "integer", 10))
    poly_layer.dataProvider().addAttributes([QgsField("contained_count", QVariant.Int)])
    poly_layer.updateFields()

    # Iterate over the polygons and set the default value for the new field
    poly_layer.startEditing()
    for f in poly_layer.getFeatures():
        poly_layer.changeAttributeValue(f.id(), fields.indexFromName("contained_count"), 0)
    poly_layer.commitChanges()

    # Create a spatial index for the layer
    index = QgsSpatialIndex()
    for f in poly_layer.getFeatures():
        index.addFeature(f)

    # Iterate over the polygons and count the number of contained polygons
    poly_layer.startEditing()
    for f in poly_layer.getFeatures():
        geom = f.geometry()
        contained_count = 0
        for id in index.intersects(geom.boundingBox()):
            other_f = next(poly_layer.getFeatures(QgsFeatureRequest(id)))
            if geom.contains(other_f.geometry()):
                contained_count += 1

        contained_count -= 1
        poly_layer.changeAttributeValue(f.id(), fields.indexFromName("contained_count"), contained_count)
    poly_layer.commitChanges()
    crs = poly_layer.crs()

    # Create two memory layers to store feature with only 1 containig poly (itself) and another for the other
    select_layer = QgsVectorLayer("Polygon?crs=" + crs.authid(), "AREA_IMOVEL_selected_because_not_contained_any", "memory")
    not_select_layer = QgsVectorLayer("Polygon?crs=" + crs.authid(), "AREA_IMOVEL_not_selected_because_containing_many_others", "memory")

    # Getting input layer fields list
    provider = poly_layer.dataProvider()
    fields = provider.fields()

    # add list fileds to memory layers
    select_provider = select_layer.dataProvider()
    select_provider.addAttributes(fields)
    select_layer.updateFields()

    not_select_provider = not_select_layer.dataProvider()
    not_select_provider.addAttributes(fields)
    not_select_layer.updateFields()

    # Filter by the number of complitly contains number of polygons
    request = QgsFeatureRequest().setFilterExpression("contained_count = 0")
    for feat in poly_layer.getFeatures(request):
        select_provider.addFeature(feat)

    request = QgsFeatureRequest().setFilterExpression("contained_count > 0")
    for feat in poly_layer.getFeatures(request):
        not_select_provider.addFeature(feat)

    # Enregistrer les nouvelles couches dans le même fichier .gpkg
    select_layer.commitChanges()
    not_select_layer.commitChanges()

    options = QgsVectorFileWriter.SaveVectorOptions()
    options.driverName = "GPKG"
    options.fileEncoding = "utf-8"
    options.layerName = select_layer.name()
    options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteLayer
    options.EditionCapability = QgsVectorFileWriter.CanAddNewLayer

    QgsVectorFileWriter.writeAsVectorFormatV3(select_layer, dest_file_path, QgsCoordinateTransformContext(), options)

    options.layerName = not_select_layer.name()
    QgsVectorFileWriter.writeAsVectorFormatV3(not_select_layer, dest_file_path, QgsCoordinateTransformContext(), options)
    return select_layer


def A1_4_b_add_overlap_values_of_polys_contained_in_one_poly(percent, input_layer, name, state):
        print("    Dealing with overlap")
        schema = QgsFields()
        schema.append(QgsField("bool_field", QVariant.Bool))
        output_file_name = f'{dest_path}/07_select_overlap/{state}/{name}_overlap.gpkg'
        create_empty_gpkg_layer(output_file_name, "temp_table", QgsWkbTypes.NoGeometry, '', schema)
        params = {
        'INPUT': input_layer,
        'LAYERS': [input_layer],
        'OUTPUT': f'ogr:dbname=\'{output_file_name}\' table="AREA_IMOVEL_with_overlap_values" (geom)'}
        output_overlap_layer_result = processing.run('native:calculatevectoroverlaps', params)
        output_overlap_layer = QgsVectorLayer(output_overlap_layer_result["OUTPUT"], '','ogr')
        fields= output_overlap_layer.fields()
        fields_name = [field.name() for field in fields]
        output_overlap_layer.startEditing()
        output_overlap_layer.renameAttribute(len(fields_name) - 1, 'overlapPc')
        output_overlap_layer.renameAttribute(len(fields_name) - 2, 'overlapArea')
        output_overlap_layer.updateFields()
        output_overlap_layer.commitChanges()
        params = {'FIELD': 'overlapArea',
         'INPUT': output_overlap_layer,
         'METHOD': 0, 'OPERATOR': 2, 'VALUE': 'percent'}
        processing.run("qgis:selectbyattribute",params)
        crs = output_overlap_layer.crs()

        # Create two memory layers to store feature with only 1 containing poly (itself) and another for the other
        crs = output_overlap_layer.crs()

        not_select_layer = QgsVectorLayer("Polygon?crs=" + crs.authid(),
                                          "AREA_IMOVEL_not_selected_because_to_much_overlap", "memory")
        not_select_layer.dataProvider().addAttributes(output_overlap_layer.fields().toList())
        not_select_layer.updateFields()
        not_select_layer.dataProvider().addFeatures(output_overlap_layer.selectedFeatures())
        not_select_layer.commitChanges()

        output_overlap_layer.invertSelection()
        select_layer = QgsVectorLayer("Polygon?crs=" + crs.authid(),
                                      f"AREA_IMOVEL_selected_because_less_than_{str(percent)}", "memory")
        select_layer.dataProvider().addAttributes(output_overlap_layer.fields().toList())
        select_layer.updateFields()
        select_layer.dataProvider().addFeatures(output_overlap_layer.selectedFeatures())
        select_layer.commitChanges()

        # Save layers in the same geopackage .gpkg


        options = QgsVectorFileWriter.SaveVectorOptions()
        options.driverName = "GPKG"
        options.fileEncoding = "utf-8"
        options.layerName = select_layer.name()
        options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteLayer
        options.EditionCapability = QgsVectorFileWriter.CanAddNewLayer

        new_selected_layer = QgsVectorFileWriter.writeAsVectorFormatV3(select_layer, output_file_name, QgsCoordinateTransformContext(), options)

        options.layerName = not_select_layer.name()
        QgsVectorFileWriter.writeAsVectorFormatV3(not_select_layer, output_file_name, QgsCoordinateTransformContext(), options)

        return new_selected_layer #str

def A2_1_0_IDF_from_everything(input_layer, from_layer, param):
        """

        """

        print(f"    IDF from {param}")
        for feature in input_layer.getFeatures():

            # Récupérer la géométrie du polygone 'NM'
            geom = feature.geometry()

            # Sélectionner tous les polygones 'RL' qui se trouvent à l'intérieur de chaque polygone 'NM'
            from_layer.selectByExpression('intersects(geometry, geom_from_wkt("POLYGON(%s)"' % geom.asWkt(), QgsVectorLayer.SetSelection)
            print(from_layer.selectedFeatureCount())
            selected_from_layer_features = [f for f in from_layer.selectedFeatures()]
            nbr_RL_features = len(selected_from_layer_features)
            if nbr_RL_features >= 1:
                print("coucou")
                feature['IDF'] = selected_from_layer_features[0]['IDF']
                feature['state'] = selected_from_layer_features[0]['state']
                feature['municipio'] = selected_from_layer_features[0]['municipio']
                feature['param'] = selected_from_layer_features[0]['param']
        input_layer.commitChanges()



if __name__ == '__main__':
    beginning = datetime.now()
    logging.info(f'Process starts @ {datetime.now().strftime("%Y-%m-%dT%Hh%M.%S")}')
    shutil.rmtree(dest_path)
    for working_dir in sorted(glob.glob(source_path + "/*/")):
        state = os.path.basename(working_dir[:-1])

        d = A1_create_dict_of_param_by_municipio(working_dir)
        i=0
        i_tot=len(d.keys())
        for key in d.keys():
            i += 1
            for value in d[key]:
                if value == 'AREA_IMOVEL':
                        print(f'{key}_{value}_{i}/{i_tot}')
                        AREA_IMOVEL_layer = QgsVectorLayer(f'{working_dir}{value}_{key}.gpkg', f'{value}_{key}', 'ogr')
                        if not AREA_IMOVEL_layer.isValid():
                            logging.error(f'Issue when trying to concatenate {working_dir}{value}_{key}.gpkg')
                        # number of imovel in imovel (poly complitly inside poly)
                        AREA_IMOVEL_layer_with_nb_poly_contained_in_selected = A1_4_a_add_number_of_polys_contained_in_one_poly(AREA_IMOVEL_layer, state)
                        # overlap
                        result = A1_4_b_add_overlap_values_of_polys_contained_in_one_poly(percent_overlap, AREA_IMOVEL_layer_with_nb_poly_contained_in_selected, f'{value}_{key}', state)

            # for each param, affect IDF if matching
            AREA_IMOVEL_layer_with_overlap_poly_selected=QgsVectorLayer(f'{result[2]}|layername={result[3]}', 'join', 'ogr')
            AREA_IMOVEL_layer_with_overlap_poly_selected.dataProvider().addAttributes(
                [QgsField("param", QVariant.String)])
            AREA_IMOVEL_layer_with_overlap_poly_selected.dataProvider().addAttributes([QgsField("IDF", QVariant.Int),
                                                                                QgsField("IDF_from", QVariant.String),
                                                                                       QgsField("nb_RL", QVariant.Int)])
            AREA_IMOVEL_layer_with_overlap_poly_selected.updateFields()
            AREA_IMOVEL_layer_with_overlap_poly_selected.commitChanges()
            list_params= []
            list_params.append('RESERVA_LEGAL') #we first want to deal with RL
            for value in d[key]:
                if value != 'AREA_IMOVEL' and value != 'RESERVA_LEGAL':
                    list_params.append(value)

            for value in list_params:
                from_layer= QgsVectorLayer(f'{source_path}/{state}/{value}_{key}.gpkg', f'{value}_{key}', 'ogr')
                # A2_1_0_IDF_from_everything(AREA_IMOVEL_layer_with_overlap_poly_selected, from_layer, value)
                ###
                print(f"    IDF from {value}")
                for feature in AREA_IMOVEL_layer_with_overlap_poly_selected.getFeatures():

                    if not feature["IDF"]:
                        sql_expression = f''' fid = {feature.id()} '''
                        AREA_IMOVEL_layer_with_overlap_poly_selected.setSubsetString(
                            sql_expression)  # This will filter out the features in the `intersect_layer`
                        processing.run("native:selectbylocation",
                                       {'INPUT': from_layer,
                                        'PREDICATE': [0],
                                        'INTERSECT': AREA_IMOVEL_layer_with_overlap_poly_selected,
                                        'METHOD': 0})
                        if value != 'RESERVA_LEGAL':
                            nb_RL = from_layer.selectedFeatureCount()
                            new_values = {
                                feature.fieldNameIndex('nb_RL'): nb_RL
                            }
                            AREA_IMOVEL_layer_with_overlap_poly_selected.dataProvider().changeAttributeValues(
                                {feature.id(): new_values})
                            AREA_IMOVEL_layer_with_overlap_poly_selected.updateFeature(feature)
                        if from_layer.selectedFeatureCount()>=1:

                            if value != 'RESERVA_LEGAL':
                                selected_from_layer_features = [f for f in from_layer.selectedFeatures()]
                                new_values = {
                                    feature.fieldNameIndex('IDF'): selected_from_layer_features[0]['IDF'],
                                    feature.fieldNameIndex('IDF_from'): value
                                }
                                AREA_IMOVEL_layer_with_overlap_poly_selected.dataProvider().changeAttributeValues(
                                    {feature.id(): new_values})
                                AREA_IMOVEL_layer_with_overlap_poly_selected.updateFeature(feature)

                            else:
                                if from_layer.selectedFeatureCount() == 1:
                                    selected_from_layer_features = [f for f in from_layer.selectedFeatures()]
                                    new_values = {
                                        feature.fieldNameIndex('IDF'): selected_from_layer_features[0]['IDF'],
                                        feature.fieldNameIndex('IDF_from'): value
                                    }
                                    AREA_IMOVEL_layer_with_overlap_poly_selected.dataProvider().changeAttributeValues(
                                        {feature.id(): new_values})
                                    AREA_IMOVEL_layer_with_overlap_poly_selected.updateFeature(feature)
                                else:
                                    area_imovel_surface = feature['area']
                                    for from_feat in from_layer.selectedFeatures():
                                        reserva_legale_area=from_feat['area']
                                        if reserva_legale_area > (area_imovel_surface * min_percent_AI_RL) \
                                                and reserva_legale_area < (area_imovel_surface * max_percent_AI_RL):
                                            new_values = {
                                                feature.fieldNameIndex('IDF'): from_feat['IDF'],
                                                feature.fieldNameIndex('IDF_from'): value
                                            }
                                            AREA_IMOVEL_layer_with_overlap_poly_selected.dataProvider().changeAttributeValues(
                                                {feature.id(): new_values})
                                            AREA_IMOVEL_layer_with_overlap_poly_selected.updateFeature(feature)
                                            break

                AREA_IMOVEL_layer_with_overlap_poly_selected.setSubsetString("")

                AREA_IMOVEL_layer_with_overlap_poly_selected.commitChanges()
                ####
            for feature in AREA_IMOVEL_layer_with_overlap_poly_selected.getFeatures():
                if not feature["IDF"]:
                    new_values = {
                        feature.fieldNameIndex('IDF'): -1,
                        feature.fieldNameIndex('IDF_from'): 'non available'
                    }
                    AREA_IMOVEL_layer_with_overlap_poly_selected.dataProvider().changeAttributeValues(
                        {feature.id(): new_values})
                    AREA_IMOVEL_layer_with_overlap_poly_selected.updateFeature(feature)


            # finally_save_file
            options = QgsVectorFileWriter.SaveVectorOptions()
            options.driverName = "GPKG"
            options.fileEncoding = "utf-8"
            options.layerName = AREA_IMOVEL_layer_with_overlap_poly_selected.name()
            dest_file_path=f"{dest_path}/08_joined/{state}/AREA_imovel_with_idf_{key}.gpkg"
            os.makedirs(os.path.dirname(dest_file_path), exist_ok=True)
            schema = QgsFields()
            schema.append(QgsField("bool_field", QVariant.Bool))
            create_empty_gpkg_layer(dest_file_path, "temp_table", QgsWkbTypes.NoGeometry, '', schema)
            QgsVectorFileWriter.writeAsVectorFormatV3(AREA_IMOVEL_layer_with_overlap_poly_selected, dest_file_path, QgsCoordinateTransformContext(), options)
    logging.info(f'Process finishes @ {datetime.now().strftime("%Y-%m-%dT%Hh%M.%S")} - duration : {str(datetime.now()-beginning)}')



