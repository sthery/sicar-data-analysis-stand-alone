#!/usr/bin/env python3
# coding: utf-8
"""
Coding:
    Sylvain Théry - UMR 5281 ART-Dev - 2022/2023
    CC-by-SA-NC
Algorithm:
    Pierre Gautereau - UMR 8586 Prodig - Paris, France
    Ludivine Eloy - UMR 5281 ART-Dev - Montpellier, France
    Sylvain Théry - UMR 5281 ART-Dev - Montpellier, France
"""


import glob
import logging
import os
import shutil
from datetime import datetime
import sys
from PyQt5.QtCore import QVariant
from qgis._core import QgsSpatialIndex, QgsFeatureRequest, QgsProject, QgsCoordinateTransformContext, \
    QgsCoordinateReferenceSystem, QgsFields, QgsProcessingFeatureSourceDefinition
from qgis.core import QgsField, QgsVectorLayer, QgsApplication, QgsVectorFileWriter, QgsFeature, QgsWkbTypes

# params
source_path1 = '/home/sylvain/Work/ART-Dev/2022/2022-00-00_Bresil/2022-10-13/2_cleaned_data/05_geom_changed/'
source_path2 = '/home/sylvain/Work/ART-Dev/2022/2022-00-00_Bresil/2022-10-13/3_imovel_process/08_joined/'
dest_path = '/home/sylvain/Work/ART-Dev/2022/2022-00-00_Bresil/2022-10-13/4_merge_analysis/'
percent=15
log_level = logging.INFO
#
# /!\ U should not change things below /!\
#
os.makedirs(os.path.dirname(dest_path), exist_ok=True)
result_file_path = dest_path + datetime.now().strftime('%Y-%m-%dT%Hh%M.%S') + "_results.csv"
log_file_path = dest_path + datetime.now().strftime('%Y-%m-%dT%Hh%M.%S') + "_run.log"

# logging stuff
logging.basicConfig(level=log_level,
                    filename=log_file_path,
                    filemode="a",
                    format='%(asctime)s - %(levelname)s - %(message)s')

# QGIS stuff
# See https://gis.stackexchange.com/a/155852/4972 for details about the prefix
QgsApplication.setPrefixPath('/usr', True)
qgs = QgsApplication([], False)
qgs.initQgis()
# Append the path where processing plugin can be found
sys.path.append('/usr/share/qgis/python/plugins/')
import processing
from processing.core.Processing import Processing

Processing.initialize()
def create_empty_gpkg_layer(gpkg_path: str, layer_name: str, geometry: int,
                            crs: str, schema: QgsFields, append: bool = False
                            ) -> None:
    """
    Create a empty layer into a gpkg file. The gpkg is created if needed, and can be overwritten if it already exists
    Taken from :
    https://gis.stackexchange.com/questions/417916/creating-empty-layers-in-a-geopackage-using-pyqgis
    Thanks to Germán Carrillo https://gis.stackexchange.com/users/4972/germ%c3%a1n-carrillo

    :param gpkg_path: geopackage file
    :type gpkg_path: str

    :param layer_name: layer to be created
    :type layer_name: str

    :param geometry: Geometry Type. Can be none.
    :type geometry: QgsWkbType

    :param crs: CRS of the geometry. Can be empty
    :type crs: str

    :param schema: Attribute table structure
    :type schema: QgsFields()

    :param append: What to do when gpkg file exists (create or overwrite layer)
    :type append: bool

    :return: None
    :rtype: None
    """
    os.makedirs((os.path.dirname(gpkg_path)), exist_ok=True)
    options = QgsVectorFileWriter.SaveVectorOptions()
    options.driverName = "GPKG"
    options.layerName = layer_name
    if append:
        options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteLayer

    writer = QgsVectorFileWriter.create(
        gpkg_path,
        schema,
        geometry,
        QgsCoordinateReferenceSystem(crs),
        QgsCoordinateTransformContext(),
        options)
    del writer


def A3_1_merge_RL():
        RL_file_list = []
        print(f'Merging RL')
        for working_dir in sorted(glob.glob(source_path1 + "/*/")):
            state = os.path.basename(working_dir[:-1])
            for filename in sorted(glob.glob(working_dir + "/*.gpkg")):
                left, right = filename.split('.')[0].rsplit("_", 1)
                if os.path.basename(left) == "RESERVA_LEGAL":
                    RL_file_list.append(f"{filename}|layername={os.path.basename(filename)[:-5]}")
        dest_file = f"{dest_path}/{state}/RESERVA_LEGAL_merged_singlepart.gpkg"
        schema = QgsFields()
        schema.append(QgsField("bool_field", QVariant.Bool))
        create_empty_gpkg_layer(dest_file, "temp_table", QgsWkbTypes.NoGeometry, '', schema)
        params = {'LAYERS': RL_file_list,
                  'CRS': None,
                  'OUTPUT': f'ogr:dbname=\'{dest_file}\' table="Reserva_legal" (geom)'}
        RL_merged = processing.run("native:mergevectorlayers", params)
        processing.run("native:spatialiteexecutesql",
                       {'DATABASE': f'{dest_file}|layername=temp_table', 'SQL': 'drop table temp_table'})
        return RL_merged


def A3_2_mergeAI():
    AI_file_list = []
    print(f'Merging AI')
    for working_dir in sorted(glob.glob(source_path2 + "/*/")):
        state = os.path.basename(working_dir[:-1])
        for filename in sorted(glob.glob(working_dir + "/*.gpkg")):
            AI_file_list.append(f"{filename}")
    dest_file = f"{dest_path}/{state}/AREA_IMOVEL_merged.gpkg"
    schema = QgsFields()
    schema.append(QgsField("bool_field", QVariant.Bool))
    create_empty_gpkg_layer(dest_file, "temp_table", QgsWkbTypes.NoGeometry, '', schema)
    params = {'LAYERS': AI_file_list,
              'CRS': None,
              'OUTPUT': f'ogr:dbname=\'{dest_file}\' table="Area_imovel" (geom)'}
    AI_merged = processing.run("native:mergevectorlayers", params)
    processing.run("native:spatialiteexecutesql",
                   {'DATABASE': f'{dest_file}|layername=temp_table', 'SQL': 'drop table temp_table'})
    return AI_merged


if __name__ == '__main__':
        beginning = datetime.now()
        logging.info(f'Process starts @ {datetime.now().strftime("%Y-%m-%dT%Hh%M.%S")}')
        shutil.rmtree(dest_path)
        result = A3_1_merge_RL()
        RL_merged_layer =QgsVectorLayer(f'{result["OUTPUT"]}', '', 'ogr')
        result= A3_2_mergeAI()
        AI_merged_layer = QgsVectorLayer(f'{result["OUTPUT"]}', '', 'ogr')

        params1 = {'FIELD': 'IDF_from',
         'INPUT': AI_merged_layer,
         'METHOD': 0, 'OPERATOR': 1, 'VALUE': 'RESERVA_LEGAL'}

        params2 = {'FIELD': 'IDF_from',
         'INPUT': AI_merged_layer,
         'METHOD': 3, 'OPERATOR': 1, 'VALUE': 'non available'}

        # processing.run("qgis:selectbyattribute", params1)
        # processing.run("qgis:selectbyattribute", params2)
        # params3 = { 'ANTIMERIDIAN_SPLIT' : False, 'GEODESIC' : False, 'GEODESIC_DISTANCE' : 1000,
        #             'HUBS' : QgsProcessingFeatureSourceDefinition(AI_merged_layer,
        #                     selectedFeaturesOnly=True, featureLimit=-1,
        #                     geometryCheck=QgsFeatureRequest.GeometryAbortOnInvalid), 'HUB_FIELD' : 'IDF',
        #                     'HUB_FIELDS' : ['COD_IMOVEL','state_AI','munici_AI'],
        #                     'OUTPUT' : 'ogr:dbname=\'/home/sylvain/Work/ART-Dev/2022/2022-00-00_Bresil/2022-10-13/4_merge_analysis/crash/LINES_AI_RL.gpkg\' table="lines_hub" (geom)',
        #                     'SPOKES' : '/home/sylvain/Work/ART-Dev/2022/2022-00-00_Bresil/2022-10-13/4_merge_analysis/crash/RESERVA_LEGAL_merged.gpkg|layername=Reserva_legal',
        #                     'SPOKE_FIELD' : 'IDF', 'SPOKE_FIELDS' : ['IDF','state','municipio'] }
        # processing.run("native:hublines", params3)
        logging.info(f'Process finishes @ {datetime.now().strftime("%Y-%m-%dT%Hh%M.%S")} - duration : {str(datetime.now()-beginning)}')





