from qgis.core import QgsProviderRegistry, QgsVectorLayer, QgsWkbTypes

directory = '/chemin/vers/les/fichiers/GPKG'

with open('output.txt', 'w') as f:
    for provider in QgsProviderRegistry.instance().providerList():
        if provider == 'ogr':
            for filename in os.listdir(directory):
                if filename.endswith('.gpkg'):
                    filepath = os.path.join(directory, filename)
                    layers = QgsProviderRegistry.instance().provider(provider).dataLayers(filepath)
                    for layer in layers:
                        if layer.isValid():
                            geometry_type = QgsWkbTypes.geometryDisplayString(layer.geometryType())
                            f.write(f'{layer.name()}, {geometry_type}\n')
#

import platform

system = platform.system()
if system == "Windows":
    print("The script is running on Windows.")
elif system == "Linux":
    print("The script is running on Linux.")
elif system == "Darwin":
    print("The script is running on macOS.")
else:
    print(f"The script is running on unknown system: {system}.")
 ###
def realcentroid2(layer, dest):
    """ generate point on surface
    taken and adaptated from Zoltan Siki's RealCentroid plugin
    https://github.com/zsiki/realcentroid/
    """
    vlayer = QgsVectorLayer(layer)
    vprovider = vlayer.dataProvider()
    dest = os.path.splitext(dest)[0]

    # Use the writeAsVectorFormat method instead of the constructor
    error = QgsVectorFileWriter.writeAsVectorFormat(vlayer, dest, "utf-8", vprovider.crs(), "GPKG")

    if error == QgsVectorFileWriter.NoError:
        print("File created successfully!")
    else:
        print("Error creating file:", error)

    features = vlayer.getFeatures()
    nElement = 0
    nError = 0
    for inFeat in features:
        nElement += 1
        inGeom = inFeat.geometry()
        if inGeom is None or not inFeat.hasGeometry() or \
                not inGeom.isGeosValid():
            nError += 1
            continue
        if inGeom.isMultipart():
            # find largest part in case of multipart
            maxarea = 0
            for part in inGeom.asGeometryCollection():
                area = part.area()
                if area > maxarea:
                    tmpGeom = part
                    maxarea = area
            inGeom = tmpGeom
        atMap = inFeat.attributes()
        outGeom = inGeom.pointOnSurface()
        outFeat = QgsFeature(vprovider.fields())
        outFeat.setGeometry(outGeom)
        outFeat.setAttributes(atMap)
        vprovider.addFeatures([outFeat])
    vlayer.updateExtents()


